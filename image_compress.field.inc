<?php

/**
 * @file
 * Written by Julian CABLE <julian.cable[AT]yahoo[DOT]com>
 * based on file_resup by
 * Written by Henri MEDOT <henri.medot[AT]absyx[DOT]fr>
 * http://www.absyx.fr
 */

/**
 * Implements hook_field_info_alter().
 */
function image_compress_field_info_alter(&$info) {
  foreach (array('image') as $type) {
    if (isset($info[$type])) {
      $info[$type]['instance_settings'] += array(
        'imgcom' => 0,
        'imgcom_max_filesize' => '',
        'imgcom_autostart' => 0,
      );
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for field_ui_field_edit_form().
 */
function image_compress_form_field_ui_field_edit_form_alter(&$form, &$form_state, $form_id) {
  if (image_compress_field_widget_support($form['#instance']['widget']['type'])) {
    $settings = $form['#instance']['settings'];

    $additions['imgcom_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Compressed image upload settings'),
      '#collapsible' => TRUE,
      '#collapsed' => !$settings['imgcom'],
      '#parents' => array('instance', 'settings'),
      '#weight' => 20,
    );

    $additions['imgcom_settings']['imgcom'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable compressed image upload'),
      '#default_value' => $settings['imgcom'],
    );

    $additions['imgcom_settings']['imgcom_max_filesize'] = array(
      '#type' => 'textfield',
      '#title' => t('Maximum upload size'),
      '#default_value' => $settings['imgcom_max_filesize'],
      '#description' => t("You may enter a value greater than the default maximum upload size above. It can exceed PHP's maximum post and file upload sizes as well."),
      '#size' => 10,
      '#element_validate' => array('_file_generic_settings_max_filesize'),
    );

    $additions['imgcom_settings']['imgcom_autostart'] = array(
      '#type' => 'checkbox',
      '#title' => t('Start upload on files added'),
      '#default_value' => $settings['imgcom_autostart'],
      '#description' => t('When checked, upload will start as soon as files are added without requiring to click <em>Upload</em>, unless some of the added files did not pass validation.'),
    );

    $form['instance']['settings'] += $additions;
  }
}

/**
 * Implements hook_field_widget_form_alter().
 */
function image_compress_field_widget_form_alter(&$element, &$form_state, $context) {
  $instance = $context['instance'];
  if (!empty($instance['settings']['imgcom']) && image_compress_field_widget_support($instance['widget']['type']) && user_access('upload via image_compress') && file_upload_max_size() >= image_compress_chunksize()) {
    $keys = element_children($element);
    $delta = end($keys);

    $cardinality = $context['field']['cardinality'];
    $element[$delta]['#image_compress_max_files'] = $cardinality != FIELD_CARDINALITY_UNLIMITED ? $cardinality - $delta : -1;

    $upload_validators = $element[$delta]['#upload_validators'];
    $upload_validators['file_validate_size'] = array(image_compress_max_filesize($instance));
    $element[$delta]['#image_compress_upload_validators'] = $upload_validators;

    $element[$delta]['#process'][] = 'image_compress_field_widget_process';
    $element[$delta]['#file_value_callbacks'][] = 'image_compress_field_widget_value';
  }
}

/**
 * #process callback for the field widget element.
 */
function image_compress_field_widget_process($element, &$form_state, $form) {
  $path = drupal_get_path('module', 'image_compress');
  $max_files = $element['#image_compress_max_files'];

  // Get the upload validators and build a new description.
  $field = field_widget_field($element, $form_state);
  $instance = field_widget_instance($element, $form_state);
  $description = $field['cardinality'] == 1 ? field_filter_xss($instance['description']) : '';
  $upload_validators = $element['#image_compress_upload_validators'];
  // exclude the file upload validation message and add our own.
  $muv = array();
  foreach($upload_validators as $key => $value) {
    if($key != 'file_validate_size') {
      $muv[$key] = $value;
    }
  }
  $description = theme('file_upload_help', array('description' => $description, 'upload_validators' => $muv));
  $description .= t('<br/>Files larger than !size.', array(
    '!size' => '<strong>' . format_size($upload_validators['file_validate_size'][0]) . '</strong>' . ' will be compressed and converted to jpeg format.'
  ));


  // Add the imgcom element.
  $element['imgcom'] = array(
    '#type' => 'hidden',
    '#value_callback' => 'image_compress_value',
    '#field_name' => $element['#field_name'],
    '#field_parents' => $element['#field_parents'],
    '#upload_location' => $element['#upload_location'],
    '#image_compress_upload_validators' => $upload_validators,
    '#attributes' => array(
      'class' => array('image-compress'),
      'data-upload-name' => $element['upload']['#name'],
      'data-upload-button-name' => $element['upload_button']['#name'],
      'data-max-filesize' => $upload_validators['file_validate_size'][0],
      'data-description' => $description,
      'data-url' => url('image_compress/upload/' . implode('/', $element['#array_parents']) . '/' . $form['form_build_id']['#value']),
      'data-drop-message' => $max_files > -1 ? format_plural($max_files, 'Drop a file here or click <em>Browse</em> below.', 'Drop up to @count files here or click <em>Browse</em> below.') : t('Drop files here or click <em>Browse</em> below.'),
    ),
    '#prefix' => '<div class="image-compress-wrapper">',
    '#suffix' => '</div>',
    '#attached' => array(
      'css' => array($path . '/image_compress.css'),
      'js' => array(
        $path . '/js/canvas-to-blob.min.js',
        $path . '/js/imgcom.min.js',
        $path . '/image_compress.js',
        array(
          'type' => 'setting',
          'data' => array('image_compress' => array('chunk_size' => image_compress_chunksize())),
        ),
      ),
    ),
  );

  // Add the extension list as a data attribute.
  if (isset($upload_validators['file_validate_extensions'][0])) {
    $extension_list = implode(',', array_filter(explode(' ', $upload_validators['file_validate_extensions'][0])));
    $element['imgcom']['#attributes']['data-extensions'] = $extension_list;
  }

  // Add the maximum number of files as a data attribute.
  if ($max_files > -1) {
    $element['imgcom']['#attributes']['data-max-files'] = $max_files;
  }

  // Add autostart as a data attribute.
  if ($instance['settings']['imgcom_autostart']) {
    $element['imgcom']['#attributes']['data-autostart'] = 'on';
  }

  $element['upload_button']['#submit'][] = 'image_compress_field_widget_submit';
  $element['#pre_render'][] = 'image_compress_field_widget_pre_render';

  return $element;
}

/**
 * #file_value_callbacks callback for the field widget element.
 */
function image_compress_field_widget_value($element, &$input, $form_state) {
  if (!empty($input['imgcom'])) {
    $imgcom_file_ids = explode(',', $input['imgcom']);
    $imgcom_file_id = reset($imgcom_file_ids);
    if ($file = image_compress_save_upload($element, $imgcom_file_id)) {
      $input['fid'] = $file->fid;
    }
  }
}

/**
 * #value_callback callback for the imgcom element.
 */
function image_compress_value($element, $input = FALSE, $form_state = array()) {
  $fids = array();

  if ($input) {
    $imgcom_file_ids = explode(',', $input);
    array_shift($imgcom_file_ids);
    if (isset($element['#attributes']['data-max-files'])) {
      $imgcom_file_ids = array_slice($imgcom_file_ids, 0, max(0, $element['#attributes']['data-max-files'] - 1));
    }
    foreach ($imgcom_file_ids as $imgcom_file_id) {
      if ($file = image_compress_save_upload($element, $imgcom_file_id)) {
        $fids[] = $file->fid;
      }
    }
  }

  return implode(',', $fids);
}

/**
 * #submit callback for the upload button of the field widget element.
 */
function image_compress_field_widget_submit($form, &$form_state) {
  $button = $form_state['triggering_element'];
  $element = drupal_array_get_nested_value($form, array_slice($button['#array_parents'], 0, -1));
  $field_name = $element['#field_name'];
  $langcode = $element['#language'];
  $parents = $element['#field_parents'];
  $field_state = field_form_get_state($parents, $field_name, $langcode, $form_state);
  $items = $field_state['items'];

  // Remove possible duplicate items.
  $fids = array();
  foreach ($items as $delta => $item) {
    if (in_array($item['fid'], $fids)) {
      unset($items[$delta]);
    }
    else {
      $fids[] = $item['fid'];
    }
  }
  $items = array_values($items);

  // Append our items.
  if (!empty($element['imgcom']['#value'])) {
    $fids = array_diff(explode(',', $element['imgcom']['#value']), $fids);
    foreach ($fids as $fid) {
      $items[] = array('fid' => $fid);
    }
  }

  drupal_array_set_nested_value($form_state['values'], array_slice($button['#array_parents'], 0, -2), $items);
  $field_state['items'] = $items;
  field_form_set_state($parents, $field_name, $langcode, $form_state, $field_state);
}

/**
 * #pre_render callback for the field widget element.
 */
function image_compress_field_widget_pre_render($element) {
  if (!empty($element['#value']['fid'])) {
    $element['imgcom']['#access'] = FALSE;
  }
  return $element;
}

/**
 * Check whether our module has support for a widget type.
 */
function image_compress_field_widget_support($widget_type) {
  $supported_types = &drupal_static(__FUNCTION__);

  if (!isset($supported_types)) {
    $supported_types = module_invoke_all('image_compress_supported_field_widget_types');
    $supported_types = array_combine($supported_types, $supported_types);
    drupal_alter('image_compress_supported_field_widget_types', $supported_types);
  }

  return !empty($supported_types[$widget_type]);
}

/**
 * Implements hook_image_compress_supported_field_widget_types().
 */
function image_compress_image_compress_supported_field_widget_types() {
  return array('file_generic', 'image_image');
}

/**
 * Get the file size limit for a field instance.
 */
function image_compress_max_filesize($instance) {
  $max_filesize = file_upload_max_size();

  if (!empty($instance['settings']['max_filesize'])) {
    $size = parse_size($instance['settings']['max_filesize']);
    if ($size < $max_filesize) {
      $max_filesize = $size;
    }
  }

  if (!empty($instance['settings']['imgcom_max_filesize'])) {
    $size = parse_size($instance['settings']['imgcom_max_filesize']);
    if ($size > $max_filesize) {
      $max_filesize = $size;
    }
  }

  return $max_filesize;
}
